/* -*- mode: c++; coding: utf-8 -*- */

//
// Copyright (c) 2012-2014 Oscar Aceña. All rights reserved.
//
// This file is part of PyEndpoint
//
// PyEndpoint is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PyEndpoint is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PyEndpoint.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef PYENDPOINT_DEBUG_H
#define PYENDPOINT_DEBUG_H

#define RED "\033[01;31m"
#define CLR "\033[00m"

#define fixme(MSG) printf(RED "FIXME:" CLR " %s (%s:%d %s)\n",	\
			  MSG, __FILE__, __LINE__, __func__); assert(false)

#ifdef DEBUG
  #define trace() printf("CPP: %s:%d %s\n", __FILE__, __LINE__, __func__)
  #define IDG
#else
  #define trace(...)
  #define IDG if(0)
#endif

#ifdef EXTRA_DEBUG
  #define IXDG
#else
  #define IXDG if(0)
#endif

#endif // PYENDPOINT_DEBUG_H
